# Form_Validator.js

A simple HTML form validation script written in pure javascript. Works with HTML's data attributes and that's where all the initializations are done. Uses two data attributes, "data-messages" attribute and "data-rules" attribute. Initialize your rules and error messages. Validates for minimum number of characters, emails, regular expressions, matching fields and non-matching fields, required fields, greater than, less than, greater than or equal to, less than or equal to, a remote validation for validating values stored in a server and this comes in handy when you for example when you want to check if a username has already been registered on the server's database before submitting the form to the server. It's pure Javascript and does not require the use of an external library like JQuery. Just download or clone the repo, link to the Form_Validator.js file in your html file, create a new instance of the Form_Validator class like below;

var validate = new Form_Validator('name_of_the_html_form_as_defined_in_the_name_attribute_of_your_html');

Initialize html data attributes like below:
<input type="text" name="username" data-rules="required|min_length_check:5" data-messages-'required-Please enter your username|min_length_check-Ensure your username has at least 5 characters'>

You can also specify an optional 'data-error' attribute that specifies the name of the html element that displays the error message to the user if validation fails. In the absence of the 'data-error' attribute, a span element is created and added to the DOM if validation fails for any of the fields. This can be shown below;
 
<input type="text" name="email" data-rules='required|valid_email' data-messages='required-Please fill in your email address|valid_email-Enter a valid email address' data-error='email_error_element'>
<span name='email_error_element'></span>

Please note that the value of the data-error attribute is the value of the name attribute of the html element that should display the error message as defined in your HTML script.

<input type="text" name="regular_expression_check" data-rules='required|reg_exp:^leonard\\**\\^joey' data-messages='required-this field is required|reg_exp-text must start with "leonard" or "joey"'>

Note: the rules and messages are separated by the pipe character, messages are indicated in this manner rule-rule's message, and rules that require parameters like min_length_check and reg_exp, the parameter(s) is/are placed on the RHS of the rule name separated by a colon eg, "reg_exp:regular_expression1\\**\\regular_expression2". Note that when validating against more than one regular expression, separate with this characters "\\**\\" without the quotation marks. The script uses this as a separator "\\**\\" when running through regular expression rules. Messages are in the form rule dash rule's message eg. valid_email-Please provide a valid email address, with no spaces in between. 

Hidden field and fields without the data-rules and data-messages attributes are ignored eg
<input type="hidden" name="something_the_user_should_not_see" value='secret'>
<input type="text" name="unimportant">

RULES
*required
*valid_email
*reg_exp
*min_length_check
*equals
*notequals
*valid_number
*valid_remote
*less_than
*greater_than
*greater_than_or_equal
*less_than_or_equal

More rules are gonna be added from time to time