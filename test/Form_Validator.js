var Form_Validator = /** @class */ (function () {
    function Form_Validator(form_name) {
        if (!form_name) {
            console.error("Please specify the HTML name attribute of the form to be validated");
            return;
        }
        if (!document.forms[form_name]) {
            return;
        }
        this.validate_object_test = {
            'required': function (value) {
                return value.trim() == '' ? false : true;
            },
            'min_length_check': function (value, count) {
                return value.trim().length < count ? false : true;
            },
            'valid_email': function (value) {
                return /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test(value.toLowerCase());
            },
            'reg_exp': function (value, reg) {
                var check = false;
                for (var i = 0; i < reg.length; i++) {
                    var testing = new String(reg[i]);
                    var re = new RegExp(testing);
                    check = check || re.test(value);
                    if (check)
                        break;
                }
                return check;
            },
            'reg_exp_dependent': function (value, target_element) {
                var country_reg_exp = {
                    "Nigeria": {
                        reg_exp: [/(^0[7-9][0-1])[0-9]{8}$/, /(^234[7-9][0-1])[0-9]{8}$/, /(^2340[7-9][0-1])[0-9]{8}$/, /(^\+234[7-9][0-1])[0-9]{8}$/, /(^\+2340[7-9][0-1])[0-9]{8}$/],
                        id_length: 10
                    },
                    "Ghana": {
                        reg_exp: [/(^2330)[0-9]{9}$/, /(^\+233)[0-9]{9}$/, /(^233)[0-9]{9}$/],
                        id_length: 12
                    },
                    "India": {
                        reg_exp: [/(^\+91)[0-9]{15}$/],
                        id_length: 15
                    },
                    "United States of America": {
                        reg_exp: [/(^\+1)[0-9]{10}$/],
                        id_length: 11
                    },
                    'Others': {
                        reg_exp: [/(^\+[1-9])[0-9]{*}$/]
                    }
                };
                var check = false;
                if (country_reg_exp.hasOwnProperty(document.querySelector('[name="' + target_element + '"]')['value'])) {
                    var reg = country_reg_exp[document.querySelector('[name="' + target_element + '"]')['value']]['reg_exp'];
                    for (var i = 0; i < reg.length; i++) {
                        var testing = new String(reg[i]);
                        var re = new RegExp(testing);
                        check = check || re.test(value);
                        if (check)
                            break;
                    }
                    return check;
                }
                var reg = country_reg_exp['Others']['reg_exp'];
                for (var i = 0; i < reg.length; i++) {
                    var testing = new String(reg[i]);
                    var re = new RegExp(testing);
                    check = check || re.test(value);
                    if (check)
                        break;
                }
                return check;
            },
            'valid_number': function (value) {
                return /^-?(?:\d+|\d{1,3}(?:,\d{3})+)?(?:\.\d+)?$/.test(value);
            },
            'equals': function (value, target_element) {
                return value === document.querySelector('[name="' + target_element + '"]')['value'];
            },
            'less_than': function (test_value, standard) {
                var test_value1 = this.remove_commas(test_value);
                var standard1 = this.remove_commas(standard);
                return Number(test_value1) < Number(standard1);
            },
            'less_than_or_equal': function (test_value, standard) {
                var test_value1 = this.remove_commas(test_value);
                var standard1 = this.remove_commas(standard);
                return Number(test_value1) <= Number(standard1);
            },
            'greater_than': function (test_value, standard) {
                var test_value1 = this.remove_commas(test_value);
                var standard1 = this.remove_commas(standard);
                return Number(test_value1) > Number(standard1);
            },
            'greater_than_or_equal': function (test_value, standard) {
                var test_value1 = this.remove_commas(test_value);
                var standard1 = this.remove_commas(standard);
                return Number(test_value1) >= Number(standard1);
            },
            'remove_commas': function (numb) {
                numb = String(numb);
                numb.trim();
                var numb1 = numb.replace(/,/g, '');
                // /[^$,\d]/
                return numb1;
            },
            'notequals': function (value, target_element) {
                return value !== document.querySelector('[name="' + target_element + '"]')['value'];
            },
            'setRequestHead': function (req, header_name, header_value) {
                req.setRequestHeader(header_name, header_value);
            },
            'valid_remote': function (remote_url, data) {
                return this.return_xml_request(remote_url, data, this.test_return_remote);
            },
            'test_return_remote': function (return_value) {
                return return_value == "true";
            },
            'return_xml_request': function (remote_url, data, callback) {
                var request = new XMLHttpRequest();
                //console.log(data);
                if (!request) {
                    try {
                        // Use the latest version of the ActiveX object if available
                        request = new ActiveXObject("Msxml2.XMLHTTP.6.0");
                    }
                    catch (e1) {
                        try {
                            // Otherwise fall back on an older version
                            request = new ActiveXObject("Msxml2.XMLHTTP.3.0");
                        }
                        catch (e2) {
                            // Otherwise, throw an error
                            throw new Error("XMLHttpRequest is not supported");
                        }
                    }
                }
                request.open('POST', remote_url);
                request.setRequestHeader('Content-Type', 'application/x-www-urlencoded');
                request.setRequestHeader('Cache-Control', 'no-cache; no-store; must-revalidate');
                if (document.querySelector('meta[name="X-CSRF-TOKEN"]'))
                    request.setRequestHeader('X-CSRF-TOKEN', document.querySelector('meta[name="X-CSRF-TOKEN"]').getAttribute('content'));
                request.onreadystatechange = function () {
                    if (this.readyState === 4 && this.status === 200) {
                        return callback(request.responseText);
                    }
                };
                request.send(data);
            }
        };
        var ultimate_form_object = this.create_form_object(form_name);
        var class_object = this;
        this.validate(ultimate_form_object, form_name);
        this.add_ev_listener(document.forms[form_name], function () {
            return class_object.validate_object(ultimate_form_object);
        }, 'onsubmit');
    }
    Form_Validator.prototype.create_form_object = function (form_name) {
        var obb = {};
        for (var i = 0; i < document.forms[form_name].length; i++) {
            if (document.forms[form_name].elements[i].getAttribute('type') == 'submit' || document.forms[form_name].elements[i].getAttribute('type') == 'hidden' || document.forms[form_name].elements[i].getAttribute('type') == 'button' || !document.forms[form_name].elements[i].dataset.rules)
                continue;
            //var count = i;
            obb[document.forms[form_name].elements[i].getAttribute('name')] = {
                'count': document.forms[form_name].elements[i].parentElement.childElementCount,
                'rules': document.forms[form_name].elements[i].dataset.rules.split('|'),
                'messages': document.forms[form_name].elements[i].dataset.messages.split('|'),
                'valid': false,
                'element': document.forms[form_name].elements[i],
                'element_name': document.forms[form_name].elements[i].getAttribute('name'),
                'valid_messages': function () {
                    var message_object = {};
                    for (var u = 0; u < this.messages; u++) {
                        message_object[this.messages[u].split('-')[0]] = this.messages[u].split('-')[1];
                    }
                    return message_object;
                }
            };
        }
        return obb;
    };
    Form_Validator.prototype.add_ev_listener = function (element, funct, ev_type) {
        element[ev_type] = funct;
    };
    Form_Validator.prototype.create_error_messages = function (obj, key) {
        var ob = {};
        var messages = obj[key]['messages'];
        for (var i = 0; i < messages.length; i++) {
            ob[messages[i].split('-')[0]] = messages[i].split('-')[1];
        }
        return ob;
    };
    Form_Validator.prototype.validate_object = function (obj) {
        var valid = true;
        var class_object = this;
        var p;
        for (p in obj) {
            var in_valid = true;
            var rules = obj[p]['rules'];
            var obj_messages = this.create_error_messages(obj, p);
            for (var i = 0; i < rules.length; i++) {
                if (rules[i] == 'required') {
                    in_valid = in_valid && class_object.validate_object_test[rules[i]](obj[p]['element'].value);
                    obj[p]['valid'] = in_valid;
                    valid = valid && in_valid;
                    if (!obj[p]['valid']) {
                        if (obj[p]['element'].dataset.error) {
                            document.querySelector('[name="' + obj[p]['element'].dataset.error + '"]').innerHTML = obj_messages[rules[i]];
                        }
                        else {
                            if (obj[p]['element'].parentElement.childElementCount > obj[p]['count']) {
                                obj[p]['element'].parentElement.lastChild.innerHTML = obj_messages[rules[i]];
                            }
                            else {
                                var span = document.createElement('p');
                                var br = document.createElement('br');
                                span.style.color = 'red';
                                span.innerHTML = obj_messages[rules[i]];
                                obj[p]['element'].parentElement.appendChild(br);
                                obj[p]['element'].parentElement.appendChild(span);
                            }
                        }
                        break;
                    }
                    else {
                        if (obj[p]['element'].dataset.error) {
                            document.querySelector('[name="' + obj[p]['element'].dataset.error + '"]').innerHTML = '';
                        }
                        else {
                            if (obj[p]['element'].parentElement.childElementCount > obj[p]['count']) {
                                obj[p]['element'].parentElement.removeChild(obj[p]['element'].parentElement.lastElementChild);
                                obj[p]['element'].parentElement.removeChild(obj[p]['element'].parentElement.lastElementChild);
                            }
                        }
                    }
                }
                if (/^min_length_check/.test(rules[i])) {
                    var min_length_value = rules[i].split(':')[0];
                    var min_length_count = rules[i].split(':')[1];
                    in_valid = in_valid && class_object.validate_object_test[min_length_value](obj[p]['element'].value, min_length_count);
                    obj[p]['valid'] = in_valid;
                    valid = valid && in_valid;
                    if (!obj[p]['valid']) {
                        if (obj[p]['element'].dataset.error) {
                            document.querySelector('[name="' + obj[p]['element'].dataset.error + '"]').innerHTML = obj_messages[min_length_value];
                        }
                        else {
                            if (obj[p]['element'].parentElement.childElementCount > obj[p]['count']) {
                                obj[p]['element'].parentElement.lastChild.innerHTML = obj_messages[min_length_value];
                            }
                            else {
                                var span = document.createElement('p');
                                var br = document.createElement('br');
                                span.style.color = 'red';
                                span.innerHTML = obj_messages[min_length_value];
                                obj[p]['element'].parentElement.appendChild(br);
                                obj[p]['element'].parentElement.appendChild(span);
                            }
                        }
                        break;
                    }
                    else {
                        if (obj[p]['element'].dataset.error) {
                            document.querySelector('[name="' + obj[p]['element'].dataset.error + '"]').innerHTML = '';
                        }
                        else {
                            if (obj[p]['element'].parentElement.childElementCount > obj[p]['count']) {
                                obj[p]['element'].parentElement.removeChild(obj[p]['element'].parentElement.lastElementChild);
                                obj[p]['element'].parentElement.removeChild(obj[p]['element'].parentElement.lastElementChild);
                            }
                        }
                    }
                }
                if (/(^equals)/.test(rules[i])) {
                    var min_length_value = rules[i].split(':')[0];
                    var min_length_count = rules[i].split(':')[1];
                    in_valid = in_valid && class_object.validate_object_test[min_length_value](obj[p]['element'].value, min_length_count);
                    obj[p]['valid'] = in_valid;
                    valid = valid && in_valid;
                    if (!obj[p]['valid']) {
                        if (obj[p]['element'].dataset.error) {
                            document.querySelector('[name="' + obj[p]['element'].dataset.error + '"]').innerHTML = obj_messages[min_length_value];
                        }
                        else {
                            if (obj[p]['element'].parentElement.childElementCount > obj[p]['count']) {
                                obj[p]['element'].parentElement.lastChild.innerHTML = obj_messages[min_length_value];
                            }
                            else {
                                var span = document.createElement('p');
                                var br = document.createElement('br');
                                span.style.color = 'red';
                                span.innerHTML = obj_messages[min_length_value];
                                obj[p]['element'].parentElement.appendChild(br);
                                obj[p]['element'].parentElement.appendChild(span);
                            }
                        }
                        break;
                    }
                    else {
                        if (obj[p]['element'].dataset.error) {
                            document.querySelector('[name="' + obj[p]['element'].dataset.error + '"]').innerHTML = '';
                        }
                        else {
                            if (obj[p]['element'].parentElement.childElementCount > obj[p]['count']) {
                                obj[p]['element'].parentElement.removeChild(obj[p]['element'].parentElement.lastElementChild);
                                obj[p]['element'].parentElement.removeChild(obj[p]['element'].parentElement.lastElementChild);
                            }
                        }
                    }
                }
                if (/(^notequals)/.test(rules[i])) {
                    var min_length_value = rules[i].split(':')[0];
                    var min_length_count = rules[i].split(':')[1];
                    in_valid = in_valid && class_object.validate_object_test[min_length_value](obj[p]['element'].value, min_length_count);
                    obj[p]['valid'] = in_valid;
                    valid = valid && in_valid;
                    if (!obj[p]['valid']) {
                        if (obj[p]['element'].dataset.error) {
                            document.querySelector('[name="' + obj[p]['element'].dataset.error + '"]').innerHTML = obj_messages[min_length_value];
                        }
                        else {
                            if (obj[p]['element'].parentElement.childElementCount > obj[p]['count']) {
                                obj[p]['element'].parentElement.lastChild.innerHTML = obj_messages[min_length_value];
                            }
                            else {
                                var span = document.createElement('p');
                                var br = document.createElement('br');
                                span.style.color = 'red';
                                span.innerHTML = obj_messages[min_length_value];
                                obj[p]['element'].parentElement.appendChild(br);
                                obj[p]['element'].parentElement.appendChild(span);
                            }
                        }
                        break;
                    }
                    else {
                        if (obj[p]['element'].dataset.error) {
                            document.querySelector('[name="' + obj[p]['element'].dataset.error + '"]').innerHTML = '';
                        }
                        else {
                            if (obj[p]['element'].parentElement.childElementCount > obj[p]['count']) {
                                obj[p]['element'].parentElement.removeChild(obj[p]['element'].parentElement.lastElementChild);
                                obj[p]['element'].parentElement.removeChild(obj[p]['element'].parentElement.lastElementChild);
                            }
                        }
                    }
                }
                if (/(^greater_than_or_equal)/.test(rules[i])) {
                    var rule = rules[i].split(':')[0];
                    var standard = rules[i].split(':')[1];
                    in_valid = in_valid && class_object.validate_object_test[rule](obj[p]['element'].value, standard);
                    obj[p]['valid'] = in_valid;
                    valid = valid && in_valid;
                    if (!obj[p]['valid']) {
                        if (obj[p]['element'].dataset.error) {
                            document.querySelector('[name="' + obj[p]['element'].dataset.error + '"]').innerHTML = obj_messages[rule];
                        }
                        else {
                            if (obj[p]['element'].parentElement.childElementCount > obj[p]['count']) {
                                obj[p]['element'].parentElement.lastChild.innerHTML = obj_messages[rule];
                            }
                            else {
                                var span = document.createElement('p');
                                var br = document.createElement('br');
                                span.style.color = 'red';
                                span.innerHTML = obj_messages[rule];
                                obj[p]['element'].parentElement.appendChild(br);
                                obj[p]['element'].parentElement.appendChild(span);
                            }
                        }
                        break;
                    }
                    else {
                        if (obj[p]['element'].dataset.error) {
                            document.querySelector('[name="' + obj[p]['element'].dataset.error + '"]').innerHTML = '';
                        }
                        else {
                            if (obj[p]['element'].parentElement.childElementCount > obj[p]['count']) {
                                obj[p]['element'].parentElement.removeChild(obj[p]['element'].parentElement.lastElementChild);
                                obj[p]['element'].parentElement.removeChild(obj[p]['element'].parentElement.lastElementChild);
                            }
                        }
                    }
                }
                if (/(^less_than_or_equal)/.test(rules[i])) {
                    var rule = rules[i].split(':')[0];
                    var standard = rules[i].split(':')[1];
                    in_valid = in_valid && class_object.validate_object_test[rule](obj[p]['element'].value, standard);
                    obj[p]['valid'] = in_valid;
                    valid = valid && in_valid;
                    if (!obj[p]['valid']) {
                        if (obj[p]['element'].dataset.error) {
                            document.querySelector('[name="' + obj[p]['element'].dataset.error + '"]').innerHTML = obj_messages[rule];
                        }
                        else {
                            if (obj[p]['element'].parentElement.childElementCount > obj[p]['count']) {
                                obj[p]['element'].parentElement.lastChild.innerHTML = obj_messages[rule];
                            }
                            else {
                                var span = document.createElement('p');
                                var br = document.createElement('br');
                                span.style.color = 'red';
                                span.innerHTML = obj_messages[rule];
                                obj[p]['element'].parentElement.appendChild(br);
                                obj[p]['element'].parentElement.appendChild(span);
                            }
                        }
                        break;
                    }
                    else {
                        if (obj[p]['element'].dataset.error) {
                            document.querySelector('[name="' + obj[p]['element'].dataset.error + '"]').innerHTML = '';
                        }
                        else {
                            if (obj[p]['element'].parentElement.childElementCount > obj[p]['count']) {
                                obj[p]['element'].parentElement.removeChild(obj[p]['element'].parentElement.lastElementChild);
                                obj[p]['element'].parentElement.removeChild(obj[p]['element'].parentElement.lastElementChild);
                            }
                        }
                    }
                }
                if (/(^greater_than)/.test(rules[i])) {
                    var rule = rules[i].split(':')[0];
                    var standard = rules[i].split(':')[1];
                    in_valid = in_valid && class_object.validate_object_test[rule](obj[p]['element'].value, standard);
                    obj[p]['valid'] = in_valid;
                    valid = valid && in_valid;
                    if (!obj[p]['valid']) {
                        if (obj[p]['element'].dataset.error) {
                            document.querySelector('[name="' + obj[p]['element'].dataset.error + '"]').innerHTML = obj_messages[rule];
                        }
                        else {
                            if (obj[p]['element'].parentElement.childElementCount > obj[p]['count']) {
                                obj[p]['element'].parentElement.lastChild.innerHTML = obj_messages[rule];
                            }
                            else {
                                var span = document.createElement('p');
                                var br = document.createElement('br');
                                span.style.color = 'red';
                                span.innerHTML = obj_messages[rule];
                                obj[p]['element'].parentElement.appendChild(br);
                                obj[p]['element'].parentElement.appendChild(span);
                            }
                        }
                        break;
                    }
                    else {
                        if (obj[p]['element'].dataset.error) {
                            document.querySelector('[name="' + obj[p]['element'].dataset.error + '"]').innerHTML = '';
                        }
                        else {
                            if (obj[p]['element'].parentElement.childElementCount > obj[p]['count']) {
                                obj[p]['element'].parentElement.removeChild(obj[p]['element'].parentElement.lastElementChild);
                                obj[p]['element'].parentElement.removeChild(obj[p]['element'].parentElement.lastElementChild);
                            }
                        }
                    }
                }
                if (/(^less_than)/.test(rules[i])) {
                    var rule = rules[i].split(':')[0];
                    var standard = rules[i].split(':')[1];
                    in_valid = in_valid && class_object.validate_object_test[rule](obj[p]['element'].value, standard);
                    obj[p]['valid'] = in_valid;
                    valid = valid && in_valid;
                    if (!obj[p]['valid']) {
                        if (obj[p]['element'].dataset.error) {
                            document.querySelector('[name="' + obj[p]['element'].dataset.error + '"]').innerHTML = obj_messages[rule];
                        }
                        else {
                            if (obj[p]['element'].parentElement.childElementCount > obj[p]['count']) {
                                obj[p]['element'].parentElement.lastChild.innerHTML = obj_messages[rule];
                            }
                            else {
                                var span = document.createElement('p');
                                var br = document.createElement('br');
                                span.style.color = 'red';
                                span.innerHTML = obj_messages[rule];
                                obj[p]['element'].parentElement.appendChild(br);
                                obj[p]['element'].parentElement.appendChild(span);
                            }
                        }
                        break;
                    }
                    else {
                        if (obj[p]['element'].dataset.error) {
                            document.querySelector('[name="' + obj[p]['element'].dataset.error + '"]').innerHTML = '';
                        }
                        else {
                            if (obj[p]['element'].parentElement.childElementCount > obj[p]['count']) {
                                obj[p]['element'].parentElement.removeChild(obj[p]['element'].parentElement.lastElementChild);
                                obj[p]['element'].parentElement.removeChild(obj[p]['element'].parentElement.lastElementChild);
                            }
                        }
                    }
                }
                /*

                if(rules[i]=='valid_remote'){
                 var search_data = "search_item="+this.dataset.remote_search+"&search_value="+this.value;
                 ultimate_form_object[key]['valid'] = ultimate_form_object[key]['valid'] || class_object.validate_object_test[rules[i]]('check_user',search_data);
                 
                 if(!ultimate_form_object[key]['valid']){
                  
                     if(this.dataset.error){
             document.querySelector('[name="'+this.dataset.error+'"]').innerHTML=obj_messages[rules[i]];
                     }else{
                 
                         if(this.parentElement.childElementCount>ultimate_form_object[key]['count']){
                    
                             this.parentElement.lastChild.innerHTML = obj_messages[rules[i]];
                          }else{
                              var span = document.createElement('p');
                              var br = document.createElement('br');
                              span.style.color = 'red';
                              span.innerHTML=obj_messages[rules[i]];
                              this.parentElement.appendChild(br);
                              this.parentElement.appendChild(span);
                          }

                     }


                 break;
                 }else{
                     if(this.dataset.error){
                 document.querySelector('[name="'+this.dataset.error+'"]').innerHTML='';
                     }else{
                         if(this.parentElement.childElementCount>ultimate_form_object[key]['count']){
                             this.parentElement.removeChild(this.parentElement.lastElementChild);
                             this.parentElement.removeChild(this.parentElement.lastElementChild);
                           
             
                         }

                     }
                             
                 }
             }

             */
                if (rules[i] == 'valid_remote') {
                    var search_data = "search_item=" + obj[p]['element'].dataset.remote + "&search_value=" + obj[p]['element'].value;
                    //  console.log(search_data);
                    in_valid = in_valid && class_object.validate_object_test[rules[i]]('check_signed_up_user', search_data);
                    obj[p]['valid'] = in_valid;
                    valid = valid && in_valid;
                    if (!obj[p]['valid']) {
                        if (obj[p]['element'].dataset.error) {
                            document.querySelector('[name="' + obj[p]['element'].dataset.error + '"]').innerHTML = obj_messages[rules[i]];
                        }
                        else {
                            if (obj[p]['element'].parentElement.childElementCount > obj[p]['count']) {
                                obj[p]['element'].parentElement.lastChild.innerHTML = obj_messages[rules[i]];
                            }
                            else {
                                var span = document.createElement('p');
                                var br = document.createElement('br');
                                span.style.color = 'red';
                                span.innerHTML = obj_messages[rules[i]];
                                obj[p]['element'].parentElement.appendChild(br);
                                obj[p]['element'].parentElement.appendChild(span);
                            }
                        }
                        break;
                    }
                    else {
                        if (obj[p]['element'].dataset.error) {
                            document.querySelector('[name="' + obj[p]['element'].dataset.error + '"]').innerHTML = '';
                        }
                        else {
                            if (obj[p]['element'].parentElement.childElementCount > obj[p]['count']) {
                                obj[p]['element'].parentElement.removeChild(obj[p]['element'].parentElement.lastElementChild);
                                obj[p]['element'].parentElement.removeChild(obj[p]['element'].parentElement.lastElementChild);
                            }
                        }
                    }
                }
                if (rules[i] == 'valid_email') {
                    in_valid = in_valid && class_object.validate_object_test[rules[i]](obj[p]['element'].value);
                    obj[p]['valid'] = in_valid;
                    valid = valid && in_valid;
                    if (!obj[p]['valid']) {
                        if (obj[p]['element'].dataset.error) {
                            document.querySelector('[name="' + obj[p]['element'].dataset.error + '"]').innerHTML = obj_messages[rules[i]];
                        }
                        else {
                            if (obj[p]['element'].parentElement.childElementCount > obj[p]['count']) {
                                obj[p]['element'].parentElement.lastChild.innerHTML = obj_messages[rules[i]];
                            }
                            else {
                                var span = document.createElement('p');
                                var br = document.createElement('br');
                                span.style.color = 'red';
                                span.innerHTML = obj_messages[rules[i]];
                                obj[p]['element'].parentElement.appendChild(br);
                                obj[p]['element'].parentElement.appendChild(span);
                            }
                        }
                        break;
                    }
                    else {
                        if (obj[p]['element'].dataset.error) {
                            document.querySelector('[name="' + obj[p]['element'].dataset.error + '"]').innerHTML = '';
                        }
                        else {
                            if (obj[p]['element'].parentElement.childElementCount > obj[p]['count']) {
                                obj[p]['element'].parentElement.removeChild(obj[p]['element'].parentElement.lastElementChild);
                                obj[p]['element'].parentElement.removeChild(obj[p]['element'].parentElement.lastElementChild);
                            }
                        }
                    }
                }
                if (/^reg_exp/.test(rules[i])) {
                    var reg_exp_value = rules[i].split(':')[0];
                    var reg_exp_array = rules[i].split(':')[1].split('\\**\\');
                    in_valid = in_valid && class_object.validate_object_test[reg_exp_value](obj[p]['element'].value, reg_exp_array);
                    obj[p]['valid'] = in_valid;
                    valid = valid && in_valid;
                    if (!obj[p]['valid']) {
                        if (obj[p]['element'].dataset.error) {
                            document.querySelector('[name="' + obj[p]['element'].dataset.error + '"]').innerHTML = obj_messages[reg_exp_value];
                        }
                        else {
                            if (obj[p]['element'].parentElement.childElementCount > obj[p]['count']) {
                                obj[p]['element'].parentElement.lastChild.innerHTML = obj_messages[reg_exp_value];
                            }
                            else {
                                var span = document.createElement('p');
                                var br = document.createElement('br');
                                span.style.color = 'red';
                                span.innerHTML = obj_messages[reg_exp_value];
                                obj[p]['element'].parentElement.appendChild(br);
                                obj[p]['element'].parentElement.appendChild(span);
                            }
                        }
                        break;
                    }
                    else {
                        if (obj[p]['element'].dataset.error) {
                            document.querySelector('[name="' + obj[p]['element'].dataset.error + '"]').innerHTML = '';
                        }
                        else {
                            if (obj[p]['element'].parentElement.childElementCount > obj[p]['count']) {
                                obj[p]['element'].parentElement.removeChild(obj[p]['element'].parentElement.lastElementChild);
                                obj[p]['element'].parentElement.removeChild(obj[p]['element'].parentElement.lastElementChild);
                            }
                        }
                    }
                }
                if (rules[i] == 'valid_number') {
                    in_valid = in_valid && class_object.validate_object_test[rules[i]](obj[p]['element'].value);
                    obj[p]['valid'] = in_valid;
                    valid = valid && in_valid;
                    if (!obj[p]['valid']) {
                        if (obj[p]['element'].dataset.error) {
                            document.querySelector('[name="' + obj[p]['element'].dataset.error + '"]').innerHTML = obj_messages[rules[i]];
                        }
                        else {
                            if (obj[p]['element'].parentElement.childElementCount > obj[p]['count']) {
                                obj[p]['element'].parentElement.lastChild.innerHTML = obj_messages[rules[i]];
                            }
                            else {
                                var span = document.createElement('p');
                                var br = document.createElement('br');
                                span.style.color = 'red';
                                span.innerHTML = obj_messages[rules[i]];
                                obj[p]['element'].parentElement.appendChild(br);
                                obj[p]['element'].parentElement.appendChild(span);
                            }
                        }
                        break;
                    }
                    else {
                        if (obj[p]['element'].dataset.error) {
                            document.querySelector('[name="' + obj[p]['element'].dataset.error + '"]').innerHTML = '';
                        }
                        else {
                            if (obj[p]['element'].parentElement.childElementCount > obj[p]['count']) {
                                obj[p]['element'].parentElement.removeChild(obj[p]['element'].parentElement.lastElementChild);
                                obj[p]['element'].parentElement.removeChild(obj[p]['element'].parentElement.lastElementChild);
                            }
                        }
                    }
                }
            }
        }
        return valid;
    };
    Form_Validator.prototype.return_event_type = function (tagname) {
        return tagname.toLowerCase() == 'select' ? 'change' : 'keyup';
    };
    Form_Validator.prototype.validate = function (ultimate_form_object, form_name) {
        var class_object = this;
        for (var i = 0; i < document.forms[form_name].length; i++) {
            if (ultimate_form_object.hasOwnProperty(document.forms[form_name].elements[i].getAttribute('name'))) {
                var count = i;
                document.forms[form_name].elements[count].onclick = function () {
                    var tag = this.tagName;
                    var key = this.getAttribute('name');
                    this.addEventListener(class_object.return_event_type(tag), function () {
                        var rules = ultimate_form_object[key]['rules'];
                        var obj_messages = class_object.create_error_messages(ultimate_form_object, key);
                        for (var i = 0; i < rules.length; i++) {
                            ultimate_form_object[key]['valid'] = false;
                            if (rules[i] == 'required') {
                                ultimate_form_object[key]['valid'] = ultimate_form_object[key]['valid'] || class_object.validate_object_test[rules[i]](this.value);
                                if (!ultimate_form_object[key]['valid']) {
                                    if (class_object.return_event_type(tag) === 'change') {
                                        if (this.dataset.error) {
                                            document.querySelector('[name="' + this.dataset.error + '"]').innerHTML = obj_messages[rules[i]];
                                        }
                                        else {
                                            if (this.parentElement.childElementCount > ultimate_form_object[key]['count']) {
                                                this.parentElement.lastChild.innerHTML = obj_messages[rules[i]];
                                            }
                                            else {
                                                var span = document.createElement('p');
                                                var br = document.createElement('br');
                                                span.style.color = 'red';
                                                span.innerHTML = obj_messages[rules[i]];
                                                this.parentElement.appendChild(br);
                                                this.parentElement.appendChild(span);
                                            }
                                        }
                                    }
                                    break;
                                }
                                else {
                                    if (this.dataset.error) {
                                        document.querySelector('[name="' + this.dataset.error + '"]').innerHTML = '';
                                    }
                                    else {
                                        if (this.parentElement.childElementCount > ultimate_form_object[key]['count']) {
                                            this.parentElement.removeChild(this.parentElement.lastElementChild);
                                            this.parentElement.removeChild(this.parentElement.lastElementChild);
                                        }
                                    }
                                }
                                continue;
                            }
                            else if (/(^min_length_check)/.test(rules[i])) {
                                var min_length_value = rules[i].split(':')[0];
                                var min_length_count = rules[i].split(':')[1];
                                ultimate_form_object[key]['valid'] = ultimate_form_object[key]['valid'] || class_object.validate_object_test[min_length_value](this.value, min_length_count);
                                if (!ultimate_form_object[key]['valid']) {
                                    if (class_object.return_event_type(tag) === 'change') {
                                        if (this.dataset.error) {
                                            document.querySelector('[name="' + this.dataset.error + '"]').innerHTML = obj_messages[min_length_value];
                                        }
                                        else {
                                            if (this.parentElement.childElementCount > ultimate_form_object[key]['count']) {
                                                this.parentElement.lastChild.innerHTML = obj_messages[min_length_value];
                                            }
                                            else {
                                                var span = document.createElement('p');
                                                var br = document.createElement('br');
                                                span.style.color = 'red';
                                                span.innerHTML = obj_messages[min_length_value];
                                                this.parentElement.appendChild(br);
                                                this.parentElement.appendChild(span);
                                            }
                                        }
                                    }
                                    break;
                                }
                                else {
                                    if (this.dataset.error) {
                                        document.querySelector('[name="' + this.dataset.error + '"]').innerHTML = '';
                                    }
                                    else {
                                        if (this.parentElement.childElementCount > ultimate_form_object[key]['count']) {
                                            this.parentElement.removeChild(this.parentElement.lastElementChild);
                                            this.parentElement.removeChild(this.parentElement.lastElementChild);
                                        }
                                    }
                                }
                            }
                            else if (rules[i] == 'valid_email') {
                                ultimate_form_object[key]['valid'] = ultimate_form_object[key]['valid'] || class_object.validate_object_test[rules[i]](this.value);
                                if (!ultimate_form_object[key]['valid']) {
                                    if (class_object.return_event_type(tag) === 'change') {
                                        if (this.dataset.error) {
                                            document.querySelector('[name="' + this.dataset.error + '"]').innerHTML = obj_messages[rules[i]];
                                        }
                                        else {
                                            if (this.parentElement.childElementCount > ultimate_form_object[key]['count']) {
                                                this.parentElement.lastChild.innerHTML = obj_messages[rules[i]];
                                            }
                                            else {
                                                var span = document.createElement('p');
                                                var br = document.createElement('br');
                                                span.style.color = 'red';
                                                span.innerHTML = obj_messages[rules[i]];
                                                this.parentElement.appendChild(br);
                                                this.parentElement.appendChild(span);
                                            }
                                        }
                                    }
                                    break;
                                }
                                else {
                                    if (this.dataset.error) {
                                        document.querySelector('[name="' + this.dataset.error + '"]').innerHTML = '';
                                    }
                                    else {
                                        if (this.parentElement.childElementCount > ultimate_form_object[key]['count']) {
                                            this.parentElement.removeChild(this.parentElement.lastElementChild);
                                            this.parentElement.removeChild(this.parentElement.lastElementChild);
                                        }
                                    }
                                }
                            }
                            else if (/(^reg_exp)/.test(rules[i])) {
                                var reg_exp_value = rules[i].split(':')[0];
                                var reg_exp_array = rules[i].split(':')[1].split('\\**\\');
                                ultimate_form_object[key]['valid'] = ultimate_form_object[key]['valid'] || class_object.validate_object_test[reg_exp_value](this.value, reg_exp_array);
                                if (!ultimate_form_object[key]['valid']) {
                                    if (class_object.return_event_type(tag) === 'change') {
                                        if (this.dataset.error) {
                                            document.querySelector('[name="' + this.dataset.error + '"]').innerHTML = obj_messages[rules[i]];
                                        }
                                        else {
                                            if (this.parentElement.childElementCount > ultimate_form_object[key]['count']) {
                                                this.parentElement.lastChild.innerHTML = obj_messages[rules[i]];
                                            }
                                            else {
                                                var span = document.createElement('p');
                                                var br = document.createElement('br');
                                                span.style.color = 'red';
                                                span.innerHTML = obj_messages[reg_exp_value];
                                                this.parentElement.appendChild(br);
                                                this.parentElement.appendChild(span);
                                            }
                                        }
                                    }
                                    break;
                                }
                                else {
                                    if (this.dataset.error) {
                                        document.querySelector('[name="' + this.dataset.error + '"]').innerHTML = '';
                                    }
                                    else {
                                        if (this.parentElement.childElementCount > ultimate_form_object[key]['count']) {
                                            this.parentElement.removeChild(this.parentElement.lastElementChild);
                                            this.parentElement.removeChild(this.parentElement.lastElementChild);
                                        }
                                    }
                                }
                            }
                            else if (/(^equals)/.test(rules[i])) {
                                var min_length_valuee = rules[i].split(':')[0];
                                var min_length_countt = rules[i].split(':')[1];
                                ultimate_form_object[key]['valid'] = ultimate_form_object[key]['valid'] || class_object.validate_object_test[min_length_valuee](this.value, min_length_countt);
                                if (!ultimate_form_object[key]['valid']) {
                                    if (class_object.return_event_type(tag) === 'change' || class_object.return_event_type(tag) === 'keyup') {
                                        if (this.dataset.error) {
                                            document.querySelector('[name="' + this.dataset.error + '"]').innerHTML = obj_messages[min_length_valuee];
                                        }
                                        else {
                                            if (this.parentElement.childElementCount > ultimate_form_object[key]['count']) {
                                                this.parentElement.lastChild.innerHTML = obj_messages[min_length_valuee];
                                            }
                                            else {
                                                var span = document.createElement('p');
                                                var br = document.createElement('br');
                                                span.style.color = 'red';
                                                span.innerHTML = obj_messages[min_length_valuee];
                                                this.parentElement.appendChild(br);
                                                this.parentElement.appendChild(span);
                                            }
                                        }
                                    }
                                    break;
                                }
                                else {
                                    if (this.dataset.error) {
                                        document.querySelector('[name="' + this.dataset.error + '"]').innerHTML = '';
                                    }
                                    else {
                                        if (this.parentElement.childElementCount > ultimate_form_object[key]['count']) {
                                            this.parentElement.removeChild(this.parentElement.lastElementChild);
                                            this.parentElement.removeChild(this.parentElement.lastElementChild);
                                        }
                                    }
                                }
                            }
                            else if (/(^greater_than_or_equal)/.test(rules[i])) {
                                var rule = rules[i].split(':')[0];
                                var standard = rules[i].split(':')[1];
                                ultimate_form_object[key]['valid'] = ultimate_form_object[key]['valid'] || class_object.validate_object_test[rule](this.value, standard);
                                if (!ultimate_form_object[key]['valid']) {
                                    if (class_object.return_event_type(tag) === 'change' || class_object.return_event_type(tag) === 'keyup') {
                                        if (this.dataset.error) {
                                            document.querySelector('[name="' + this.dataset.error + '"]').innerHTML = obj_messages[rule];
                                        }
                                        else {
                                            if (this.parentElement.childElementCount > ultimate_form_object[key]['count']) {
                                                this.parentElement.lastChild.innerHTML = obj_messages[rule];
                                            }
                                            else {
                                                var span = document.createElement('p');
                                                var br = document.createElement('br');
                                                span.style.color = 'red';
                                                span.innerHTML = obj_messages[rule];
                                                this.parentElement.appendChild(br);
                                                this.parentElement.appendChild(span);
                                            }
                                        }
                                    }
                                    break;
                                }
                                else {
                                    if (this.dataset.error) {
                                        document.querySelector('[name="' + this.dataset.error + '"]').innerHTML = '';
                                    }
                                    else {
                                        if (this.parentElement.childElementCount > ultimate_form_object[key]['count']) {
                                            this.parentElement.removeChild(this.parentElement.lastElementChild);
                                            this.parentElement.removeChild(this.parentElement.lastElementChild);
                                        }
                                    }
                                }
                            }
                            else if (/(^less_than_or_equal)/.test(rules[i])) {
                                var rule = rules[i].split(':')[0];
                                var standard = rules[i].split(':')[1];
                                ultimate_form_object[key]['valid'] = ultimate_form_object[key]['valid'] || class_object.validate_object_test[rule](this.value, standard);
                                if (!ultimate_form_object[key]['valid']) {
                                    if (class_object.return_event_type(tag) === 'change' || class_object.return_event_type(tag) === 'keyup') {
                                        if (this.dataset.error) {
                                            document.querySelector('[name="' + this.dataset.error + '"]').innerHTML = obj_messages[rule];
                                        }
                                        else {
                                            if (this.parentElement.childElementCount > ultimate_form_object[key]['count']) {
                                                this.parentElement.lastChild.innerHTML = obj_messages[rule];
                                            }
                                            else {
                                                var span = document.createElement('p');
                                                var br = document.createElement('br');
                                                span.style.color = 'red';
                                                span.innerHTML = obj_messages[rule];
                                                this.parentElement.appendChild(br);
                                                this.parentElement.appendChild(span);
                                            }
                                        }
                                    }
                                    break;
                                }
                                else {
                                    if (this.dataset.error) {
                                        document.querySelector('[name="' + this.dataset.error + '"]').innerHTML = '';
                                    }
                                    else {
                                        if (this.parentElement.childElementCount > ultimate_form_object[key]['count']) {
                                            this.parentElement.removeChild(this.parentElement.lastElementChild);
                                            this.parentElement.removeChild(this.parentElement.lastElementChild);
                                        }
                                    }
                                }
                            }
                            else if (/(^greater_than)/.test(rules[i])) {
                                var rule = rules[i].split(':')[0];
                                var standard = rules[i].split(':')[1];
                                ultimate_form_object[key]['valid'] = ultimate_form_object[key]['valid'] || class_object.validate_object_test[rule](this.value, standard);
                                if (!ultimate_form_object[key]['valid']) {
                                    if (class_object.return_event_type(tag) === 'change' || class_object.return_event_type(tag) === 'keyup') {
                                        if (this.dataset.error) {
                                            document.querySelector('[name="' + this.dataset.error + '"]').innerHTML = obj_messages[rule];
                                        }
                                        else {
                                            if (this.parentElement.childElementCount > ultimate_form_object[key]['count']) {
                                                this.parentElement.lastChild.innerHTML = obj_messages[rule];
                                            }
                                            else {
                                                var span = document.createElement('p');
                                                var br = document.createElement('br');
                                                span.style.color = 'red';
                                                span.innerHTML = obj_messages[rule];
                                                this.parentElement.appendChild(br);
                                                this.parentElement.appendChild(span);
                                            }
                                        }
                                    }
                                    break;
                                }
                                else {
                                    if (this.dataset.error) {
                                        document.querySelector('[name="' + this.dataset.error + '"]').innerHTML = '';
                                    }
                                    else {
                                        if (this.parentElement.childElementCount > ultimate_form_object[key]['count']) {
                                            this.parentElement.removeChild(this.parentElement.lastElementChild);
                                            this.parentElement.removeChild(this.parentElement.lastElementChild);
                                        }
                                    }
                                }
                            }
                            else if (/(^less_than)/.test(rules[i])) {
                                var rule = rules[i].split(':')[0];
                                var standard = rules[i].split(':')[1];
                                ultimate_form_object[key]['valid'] = ultimate_form_object[key]['valid'] || class_object.validate_object_test[rule](this.value, standard);
                                if (!ultimate_form_object[key]['valid']) {
                                    if (class_object.return_event_type(tag) === 'change' || class_object.return_event_type(tag) === 'keyup') {
                                        if (this.dataset.error) {
                                            document.querySelector('[name="' + this.dataset.error + '"]').innerHTML = obj_messages[rule];
                                        }
                                        else {
                                            if (this.parentElement.childElementCount > ultimate_form_object[key]['count']) {
                                                this.parentElement.lastChild.innerHTML = obj_messages[rule];
                                            }
                                            else {
                                                var span = document.createElement('p');
                                                var br = document.createElement('br');
                                                span.style.color = 'red';
                                                span.innerHTML = obj_messages[rule];
                                                this.parentElement.appendChild(br);
                                                this.parentElement.appendChild(span);
                                            }
                                        }
                                    }
                                    break;
                                }
                                else {
                                    if (this.dataset.error) {
                                        document.querySelector('[name="' + this.dataset.error + '"]').innerHTML = '';
                                    }
                                    else {
                                        if (this.parentElement.childElementCount > ultimate_form_object[key]['count']) {
                                            this.parentElement.removeChild(this.parentElement.lastElementChild);
                                            this.parentElement.removeChild(this.parentElement.lastElementChild);
                                        }
                                    }
                                }
                            }
                            else if (/(^notequals)/.test(rules[i])) {
                                var min_length_valuee = rules[i].split(':')[0];
                                var min_length_countt = rules[i].split(':')[1];
                                ultimate_form_object[key]['valid'] = ultimate_form_object[key]['valid'] || class_object.validate_object_test[min_length_valuee](this.value, min_length_countt);
                                if (!ultimate_form_object[key]['valid']) {
                                    if (class_object.return_event_type(tag) === 'change' || class_object.return_event_type(tag) === 'keyup') {
                                        if (this.dataset.error) {
                                            document.querySelector('[name="' + this.dataset.error + '"]').innerHTML = obj_messages[min_length_valuee];
                                        }
                                        else {
                                            if (this.parentElement.childElementCount > ultimate_form_object[key]['count']) {
                                                this.parentElement.lastChild.innerHTML = obj_messages[min_length_valuee];
                                            }
                                            else {
                                                var span = document.createElement('p');
                                                var br = document.createElement('br');
                                                span.style.color = 'red';
                                                span.innerHTML = obj_messages[min_length_valuee];
                                                this.parentElement.appendChild(br);
                                                this.parentElement.appendChild(span);
                                            }
                                        }
                                    }
                                    break;
                                }
                                else {
                                    if (this.dataset.error) {
                                        document.querySelector('[name="' + this.dataset.error + '"]').innerHTML = '';
                                    }
                                    else {
                                        if (this.parentElement.childElementCount > ultimate_form_object[key]['count']) {
                                            this.parentElement.removeChild(this.parentElement.lastElementChild);
                                            this.parentElement.removeChild(this.parentElement.lastElementChild);
                                        }
                                    }
                                }
                            }
                            else if (rules[i] == 'valid_number') {
                                ultimate_form_object[key]['valid'] = ultimate_form_object[key]['valid'] || class_object.validate_object_test[rules[i]](this.value);
                                if (!ultimate_form_object[key]['valid']) {
                                    if (this.return_event_type(tag) === 'change') {
                                        if (this.dataset.error) {
                                            document.querySelector('[name="' + this.dataset.error + '"]').innerHTML = obj_messages[rules[i]];
                                        }
                                        else {
                                            if (this.parentElement.childElementCount > ultimate_form_object[key]['count']) {
                                                this.parentElement.lastChild.innerHTML = obj_messages[rules[i]];
                                            }
                                            else {
                                                var span = document.createElement('p');
                                                var br = document.createElement('br');
                                                span.style.color = 'red';
                                                span.innerHTML = obj_messages[rules[i]];
                                                this.parentElement.appendChild(br);
                                                this.parentElement.appendChild(span);
                                            }
                                        }
                                    }
                                    break;
                                }
                                else {
                                    if (this.dataset.error) {
                                        document.querySelector('[name="' + this.dataset.error + '"]').innerHTML = '';
                                    }
                                    else {
                                        if (this.parentElement.childElementCount > ultimate_form_object[key]['count']) {
                                            this.parentElement.removeChild(this.parentElement.lastElementChild);
                                            this.parentElement.removeChild(this.parentElement.lastElementChild);
                                        }
                                    }
                                }
                            }
                        }
                    });
                    this.addEventListener('blur', function () {
                        var rules = ultimate_form_object[key]['rules'];
                        var obj_messages = class_object.create_error_messages(ultimate_form_object, key);
                        for (var i = 0; i < rules.length; i++) {
                            ultimate_form_object[key]['valid'] = false;
                            if (rules[i] == 'required') {
                                ultimate_form_object[key]['valid'] = ultimate_form_object[key]['valid'] || class_object.validate_object_test[rules[i]](this.value);
                                if (!ultimate_form_object[key]['valid']) {
                                    if (this.dataset.error) {
                                        document.querySelector('[name="' + this.dataset.error + '"]').innerHTML = obj_messages[rules[i]];
                                    }
                                    else {
                                        if (this.parentElement.childElementCount > ultimate_form_object[key]['count']) {
                                            this.parentElement.lastChild.innerHTML = obj_messages[rules[i]];
                                        }
                                        else {
                                            var span = document.createElement('p');
                                            var br = document.createElement('br');
                                            span.style.color = 'red';
                                            span.innerHTML = obj_messages[rules[i]];
                                            this.parentElement.appendChild(br);
                                            this.parentElement.appendChild(span);
                                        }
                                    }
                                    break;
                                }
                                else {
                                    if (this.dataset.error) {
                                        document.querySelector('[name="' + this.dataset.error + '"]').innerHTML = '';
                                    }
                                    else {
                                        if (this.parentElement.childElementCount > ultimate_form_object[key]['count']) {
                                            this.parentElement.removeChild(this.parentElement.lastElementChild);
                                            this.parentElement.removeChild(this.parentElement.lastElementChild);
                                        }
                                    }
                                }
                                continue;
                            }
                            else if (/(^min_length_check)/.test(rules[i])) {
                                var min_length_value = rules[i].split(':')[0];
                                var min_length_count = rules[i].split(':')[1];
                                ultimate_form_object[key]['valid'] = ultimate_form_object[key]['valid'] || class_object.validate_object_test[min_length_value](this.value, min_length_count);
                                if (!ultimate_form_object[key]['valid']) {
                                    if (this.dataset.error) {
                                        document.querySelector('[name="' + this.dataset.error + '"]').innerHTML = obj_messages[min_length_value];
                                        // console.log(obj_messages[min_length_value]);
                                        // console.log(obj_messages);
                                    }
                                    else {
                                        if (this.parentElement.childElementCount > ultimate_form_object[key]['count']) {
                                            this.parentElement.lastChild.innerHTML = obj_messages[min_length_value];
                                        }
                                        else {
                                            var span = document.createElement('p');
                                            var br = document.createElement('br');
                                            span.style.color = 'red';
                                            span.innerHTML = obj_messages[min_length_value];
                                            this.parentElement.appendChild(br);
                                            this.parentElement.appendChild(span);
                                        }
                                    }
                                    break;
                                }
                                else {
                                    if (this.dataset.error) {
                                        document.querySelector('[name="' + this.dataset.error + '"]').innerHTML = '';
                                    }
                                    else {
                                        if (this.parentElement.childElementCount > ultimate_form_object[key]['count']) {
                                            this.parentElement.removeChild(this.parentElement.lastElementChild);
                                            this.parentElement.removeChild(this.parentElement.lastElementChild);
                                        }
                                    }
                                }
                            }
                            else if (rules[i] == 'valid_email') {
                                ultimate_form_object[key]['valid'] = ultimate_form_object[key]['valid'] || class_object.validate_object_test[rules[i]](this.value);
                                if (!ultimate_form_object[key]['valid']) {
                                    if (this.dataset.error) {
                                        document.querySelector('[name="' + this.dataset.error + '"]').innerHTML = obj_messages[rules[i]];
                                    }
                                    else {
                                        if (this.parentElement.childElementCount > ultimate_form_object[key]['count']) {
                                            this.parentElement.lastChild.innerHTML = obj_messages[rules[i]];
                                        }
                                        else {
                                            var span = document.createElement('p');
                                            var br = document.createElement('br');
                                            span.style.color = 'red';
                                            span.innerHTML = obj_messages[rules[i]];
                                            this.parentElement.appendChild(br);
                                            this.parentElement.appendChild(span);
                                        }
                                    }
                                    break;
                                }
                                else {
                                    if (this.dataset.error) {
                                        document.querySelector('[name="' + this.dataset.error + '"]').innerHTML = '';
                                    }
                                    else {
                                        if (this.parentElement.childElementCount > ultimate_form_object[key]['count']) {
                                            this.parentElement.removeChild(this.parentElement.lastElementChild);
                                            this.parentElement.removeChild(this.parentElement.lastElementChild);
                                        }
                                    }
                                }
                            }
                            else if (/(^reg_exp)/.test(rules[i])) {
                                var reg_exp_value = rules[i].split(':')[0];
                                var reg_exp_array = rules[i].split(':')[1].split('\\**\\');
                                ultimate_form_object[key]['valid'] = ultimate_form_object[key]['valid'] || class_object.validate_object_test[reg_exp_value](this.value, reg_exp_array);
                                if (!ultimate_form_object[key]['valid']) {
                                    if (this.dataset.error) {
                                        document.querySelector('[name="' + this.dataset.error + '"]').innerHTML = obj_messages[reg_exp_value];
                                    }
                                    else {
                                        if (this.parentElement.childElementCount > ultimate_form_object[key]['count']) {
                                            this.parentElement.lastChild.innerHTML = obj_messages[rules[i]];
                                        }
                                        else {
                                            var span = document.createElement('p');
                                            var br = document.createElement('br');
                                            span.style.color = 'red';
                                            span.innerHTML = obj_messages[reg_exp_value];
                                            this.parentElement.appendChild(br);
                                            this.parentElement.appendChild(span);
                                        }
                                    }
                                    break;
                                }
                                else {
                                    if (this.dataset.error) {
                                        document.querySelector('[name="' + this.dataset.error + '"]').innerHTML = '';
                                    }
                                    else {
                                        if (this.parentElement.childElementCount > ultimate_form_object[key]['count']) {
                                            this.parentElement.removeChild(this.parentElement.lastElementChild);
                                            this.parentElement.removeChild(this.parentElement.lastElementChild);
                                        }
                                    }
                                }
                            }
                            else if (/(^equals)/.test(rules[i])) {
                                var min_length_valuee = rules[i].split(':')[0];
                                var min_length_countt = rules[i].split(':')[1];
                                ultimate_form_object[key]['valid'] = ultimate_form_object[key]['valid'] || class_object.validate_object_test[min_length_valuee](this.value, min_length_countt);
                                if (!ultimate_form_object[key]['valid']) {
                                    if (this.dataset.error) {
                                        document.querySelector('[name="' + this.dataset.error + '"]').innerHTML = obj_messages[min_length_valuee];
                                    }
                                    else {
                                        if (this.parentElement.childElementCount > ultimate_form_object[key]['count']) {
                                            this.parentElement.lastChild.innerHTML = obj_messages[min_length_valuee];
                                        }
                                        else {
                                            var span = document.createElement('p');
                                            var br = document.createElement('br');
                                            span.style.color = 'red';
                                            span.innerHTML = obj_messages[min_length_valuee];
                                            this.parentElement.appendChild(br);
                                            this.parentElement.appendChild(span);
                                        }
                                    }
                                    break;
                                }
                                else {
                                    if (this.dataset.error) {
                                        document.querySelector('[name="' + this.dataset.error + '"]').innerHTML = '';
                                    }
                                    else {
                                        if (this.parentElement.childElementCount > ultimate_form_object[key]['count']) {
                                            this.parentElement.removeChild(this.parentElement.lastElementChild);
                                            this.parentElement.removeChild(this.parentElement.lastElementChild);
                                        }
                                    }
                                }
                            }
                            else if (/(^less_than_or_equal)/.test(rules[i])) {
                                var rule = rules[i].split(':')[0];
                                var standard = rules[i].split(':')[1];
                                ultimate_form_object[key]['valid'] = ultimate_form_object[key]['valid'] || class_object.validate_object_test[rule](this.value, standard);
                                //ultimate_form_object[key]['valid'] = ultimate_form_object[key]['valid'] || class_object.validate_object_test[min_length_valuee](this.value,min_length_countt);
                                if (!ultimate_form_object[key]['valid']) {
                                    if (this.dataset.error) {
                                        document.querySelector('[name="' + this.dataset.error + '"]').innerHTML = obj_messages[rule];
                                    }
                                    else {
                                        if (this.parentElement.childElementCount > ultimate_form_object[key]['count']) {
                                            this.parentElement.lastChild.innerHTML = obj_messages[rule];
                                        }
                                        else {
                                            var span = document.createElement('p');
                                            var br = document.createElement('br');
                                            span.style.color = 'red';
                                            span.innerHTML = obj_messages[rule];
                                            this.parentElement.appendChild(br);
                                            this.parentElement.appendChild(span);
                                        }
                                    }
                                    break;
                                }
                                else {
                                    if (this.dataset.error) {
                                        document.querySelector('[name="' + this.dataset.error + '"]').innerHTML = '';
                                    }
                                    else {
                                        if (this.parentElement.childElementCount > ultimate_form_object[key]['count']) {
                                            this.parentElement.removeChild(this.parentElement.lastElementChild);
                                            this.parentElement.removeChild(this.parentElement.lastElementChild);
                                        }
                                    }
                                }
                            }
                            else if (/(^greater_than_or_equal)/.test(rules[i])) {
                                var rule = rules[i].split(':')[0];
                                var standard = rules[i].split(':')[1];
                                ultimate_form_object[key]['valid'] = ultimate_form_object[key]['valid'] || class_object.validate_object_test[rule](this.value, standard);
                                //ultimate_form_object[key]['valid'] = ultimate_form_object[key]['valid'] || class_object.validate_object_test[min_length_valuee](this.value,min_length_countt);
                                if (!ultimate_form_object[key]['valid']) {
                                    if (this.dataset.error) {
                                        document.querySelector('[name="' + this.dataset.error + '"]').innerHTML = obj_messages[rule];
                                    }
                                    else {
                                        if (this.parentElement.childElementCount > ultimate_form_object[key]['count']) {
                                            this.parentElement.lastChild.innerHTML = obj_messages[rule];
                                        }
                                        else {
                                            var span = document.createElement('p');
                                            var br = document.createElement('br');
                                            span.style.color = 'red';
                                            span.innerHTML = obj_messages[rule];
                                            this.parentElement.appendChild(br);
                                            this.parentElement.appendChild(span);
                                        }
                                    }
                                    break;
                                }
                                else {
                                    if (this.dataset.error) {
                                        document.querySelector('[name="' + this.dataset.error + '"]').innerHTML = '';
                                    }
                                    else {
                                        if (this.parentElement.childElementCount > ultimate_form_object[key]['count']) {
                                            this.parentElement.removeChild(this.parentElement.lastElementChild);
                                            this.parentElement.removeChild(this.parentElement.lastElementChild);
                                        }
                                    }
                                }
                            }
                            else if (/(^less_than)/.test(rules[i])) {
                                var rule = rules[i].split(':')[0];
                                var standard = rules[i].split(':')[1];
                                ultimate_form_object[key]['valid'] = ultimate_form_object[key]['valid'] || class_object.validate_object_test[rule](this.value, standard);
                                //ultimate_form_object[key]['valid'] = ultimate_form_object[key]['valid'] || class_object.validate_object_test[min_length_valuee](this.value,min_length_countt);
                                if (!ultimate_form_object[key]['valid']) {
                                    if (this.dataset.error) {
                                        document.querySelector('[name="' + this.dataset.error + '"]').innerHTML = obj_messages[rule];
                                    }
                                    else {
                                        if (this.parentElement.childElementCount > ultimate_form_object[key]['count']) {
                                            this.parentElement.lastChild.innerHTML = obj_messages[rule];
                                        }
                                        else {
                                            var span = document.createElement('p');
                                            var br = document.createElement('br');
                                            span.style.color = 'red';
                                            span.innerHTML = obj_messages[rule];
                                            this.parentElement.appendChild(br);
                                            this.parentElement.appendChild(span);
                                        }
                                    }
                                    break;
                                }
                                else {
                                    if (this.dataset.error) {
                                        document.querySelector('[name="' + this.dataset.error + '"]').innerHTML = '';
                                    }
                                    else {
                                        if (this.parentElement.childElementCount > ultimate_form_object[key]['count']) {
                                            this.parentElement.removeChild(this.parentElement.lastElementChild);
                                            this.parentElement.removeChild(this.parentElement.lastElementChild);
                                        }
                                    }
                                }
                            }
                            else if (/(^greater_than)/.test(rules[i])) {
                                var rule = rules[i].split(':')[0];
                                var standard = rules[i].split(':')[1];
                                ultimate_form_object[key]['valid'] = ultimate_form_object[key]['valid'] || class_object.validate_object_test[rule](this.value, standard);
                                //ultimate_form_object[key]['valid'] = ultimate_form_object[key]['valid'] || class_object.validate_object_test[min_length_valuee](this.value,min_length_countt);
                                if (!ultimate_form_object[key]['valid']) {
                                    if (this.dataset.error) {
                                        document.querySelector('[name="' + this.dataset.error + '"]').innerHTML = obj_messages[rule];
                                    }
                                    else {
                                        if (this.parentElement.childElementCount > ultimate_form_object[key]['count']) {
                                            this.parentElement.lastChild.innerHTML = obj_messages[rule];
                                        }
                                        else {
                                            var span = document.createElement('p');
                                            var br = document.createElement('br');
                                            span.style.color = 'red';
                                            span.innerHTML = obj_messages[rule];
                                            this.parentElement.appendChild(br);
                                            this.parentElement.appendChild(span);
                                        }
                                    }
                                    break;
                                }
                                else {
                                    if (this.dataset.error) {
                                        document.querySelector('[name="' + this.dataset.error + '"]').innerHTML = '';
                                    }
                                    else {
                                        if (this.parentElement.childElementCount > ultimate_form_object[key]['count']) {
                                            this.parentElement.removeChild(this.parentElement.lastElementChild);
                                            this.parentElement.removeChild(this.parentElement.lastElementChild);
                                        }
                                    }
                                }
                            }
                            else if (/(^notequals)/.test(rules[i])) {
                                var min_length_valuee = rules[i].split(':')[0];
                                var min_length_countt = rules[i].split(':')[1];
                                ultimate_form_object[key]['valid'] = ultimate_form_object[key]['valid'] || class_object.validate_object_test[min_length_valuee](this.value, min_length_countt);
                                if (!ultimate_form_object[key]['valid']) {
                                    if (this.dataset.error) {
                                        document.querySelector('[name="' + this.dataset.error + '"]').innerHTML = obj_messages[min_length_valuee];
                                    }
                                    else {
                                        if (this.parentElement.childElementCount > ultimate_form_object[key]['count']) {
                                            this.parentElement.lastChild.innerHTML = obj_messages[min_length_valuee];
                                        }
                                        else {
                                            var span = document.createElement('p');
                                            var br = document.createElement('br');
                                            span.style.color = 'red';
                                            span.innerHTML = obj_messages[min_length_valuee];
                                            this.parentElement.appendChild(br);
                                            this.parentElement.appendChild(span);
                                        }
                                    }
                                    break;
                                }
                                else {
                                    if (this.dataset.error) {
                                        document.querySelector('[name="' + this.dataset.error + '"]').innerHTML = '';
                                    }
                                    else {
                                        if (this.parentElement.childElementCount > ultimate_form_object[key]['count']) {
                                            this.parentElement.removeChild(this.parentElement.lastElementChild);
                                            this.parentElement.removeChild(this.parentElement.lastElementChild);
                                        }
                                    }
                                }
                            }
                            else if (rules[i] == 'valid_number') {
                                ultimate_form_object[key]['valid'] = ultimate_form_object[key]['valid'] || class_object.validate_object_test[rules[i]](this.value);
                                if (!ultimate_form_object[key]['valid']) {
                                    if (this.dataset.error) {
                                        document.querySelector('[name="' + this.dataset.error + '"]').innerHTML = obj_messages[rules[i]];
                                    }
                                    else {
                                        if (this.parentElement.childElementCount > ultimate_form_object[key]['count']) {
                                            this.parentElement.lastChild.innerHTML = obj_messages[rules[i]];
                                        }
                                        else {
                                            var span = document.createElement('p');
                                            var br = document.createElement('br');
                                            span.style.color = 'red';
                                            span.innerHTML = obj_messages[rules[i]];
                                            this.parentElement.appendChild(br);
                                            this.parentElement.appendChild(span);
                                        }
                                    }
                                    break;
                                }
                                else {
                                    if (this.dataset.error) {
                                        document.querySelector('[name="' + this.dataset.error + '"]').innerHTML = '';
                                    }
                                    else {
                                        if (this.parentElement.childElementCount > ultimate_form_object[key]['count']) {
                                            this.parentElement.removeChild(this.parentElement.lastElementChild);
                                            this.parentElement.removeChild(this.parentElement.lastElementChild);
                                        }
                                    }
                                }
                            }
                        }
                    });
                };
                document.forms[form_name].elements[count].onfocus = function () {
                    var tag = this.tagName;
                    var key = this.getAttribute('name');
                    this.addEventListener(class_object.return_event_type(tag), function () {
                        var rules = ultimate_form_object[key]['rules'];
                        var obj_messages = class_object.create_error_messages(ultimate_form_object, key);
                        for (var i = 0; i < rules.length; i++) {
                            ultimate_form_object[key]['valid'] = false;
                            if (rules[i] == 'required') {
                                ultimate_form_object[key]['valid'] = ultimate_form_object[key]['valid'] || class_object.validate_object_test[rules[i]](this.value);
                                if (!ultimate_form_object[key]['valid']) {
                                    if (class_object.return_event_type(tag) === 'change') {
                                        if (this.dataset.error) {
                                            document.querySelector('[name="' + this.dataset.error + '"]').innerHTML = obj_messages[rules[i]];
                                        }
                                        else {
                                            if (this.parentElement.childElementCount > ultimate_form_object[key]['count']) {
                                                this.parentElement.lastChild.innerHTML = obj_messages[rules[i]];
                                            }
                                            else {
                                                var span = document.createElement('p');
                                                var br = document.createElement('br');
                                                span.style.color = 'red';
                                                span.innerHTML = obj_messages[rules[i]];
                                                this.parentElement.appendChild(br);
                                                this.parentElement.appendChild(span);
                                            }
                                        }
                                    }
                                    break;
                                }
                                else {
                                    if (this.dataset.error) {
                                        document.querySelector('[name="' + this.dataset.error + '"]').innerHTML = '';
                                    }
                                    else {
                                        if (this.parentElement.childElementCount > ultimate_form_object[key]['count']) {
                                            this.parentElement.removeChild(this.parentElement.lastElementChild);
                                            this.parentElement.removeChild(this.parentElement.lastElementChild);
                                        }
                                    }
                                }
                                continue;
                            }
                            else if (/(^min_length_check)/.test(rules[i])) {
                                var min_length_value = rules[i].split(':')[0];
                                var min_length_count = rules[i].split(':')[1];
                                ultimate_form_object[key]['valid'] = ultimate_form_object[key]['valid'] || class_object.validate_object_test[min_length_value](this.value, min_length_count);
                                if (!ultimate_form_object[key]['valid']) {
                                    if (class_object.return_event_type(tag) === 'change') {
                                        if (this.dataset.error) {
                                            document.querySelector('[name="' + this.dataset.error + '"]').innerHTML = obj_messages[min_length_value];
                                        }
                                        else {
                                            if (this.parentElement.childElementCount > ultimate_form_object[key]['count']) {
                                                this.parentElement.lastChild.innerHTML = obj_messages[min_length_value];
                                            }
                                            else {
                                                var span = document.createElement('p');
                                                var br = document.createElement('br');
                                                span.style.color = 'red';
                                                span.innerHTML = obj_messages[min_length_value];
                                                this.parentElement.appendChild(br);
                                                this.parentElement.appendChild(span);
                                            }
                                        }
                                    }
                                    break;
                                }
                                else {
                                    if (this.dataset.error) {
                                        document.querySelector('[name="' + this.dataset.error + '"]').innerHTML = '';
                                    }
                                    else {
                                        if (this.parentElement.childElementCount > ultimate_form_object[key]['count']) {
                                            this.parentElement.removeChild(this.parentElement.lastElementChild);
                                            this.parentElement.removeChild(this.parentElement.lastElementChild);
                                        }
                                    }
                                }
                            }
                            else if (rules[i] == 'valid_email') {
                                ultimate_form_object[key]['valid'] = ultimate_form_object[key]['valid'] || class_object.validate_object_test[rules[i]](this.value);
                                if (!ultimate_form_object[key]['valid']) {
                                    if (class_object.return_event_type(tag) === 'change') {
                                        if (this.dataset.error) {
                                            document.querySelector('[name="' + this.dataset.error + '"]').innerHTML = obj_messages[rules[i]];
                                        }
                                        else {
                                            if (this.parentElement.childElementCount > ultimate_form_object[key]['count']) {
                                                this.parentElement.lastChild.innerHTML = obj_messages[rules[i]];
                                            }
                                            else {
                                                var span = document.createElement('p');
                                                var br = document.createElement('br');
                                                span.style.color = 'red';
                                                span.innerHTML = obj_messages[rules[i]];
                                                this.parentElement.appendChild(br);
                                                this.parentElement.appendChild(span);
                                            }
                                        }
                                    }
                                    break;
                                }
                                else {
                                    if (this.dataset.error) {
                                        document.querySelector('[name="' + this.dataset.error + '"]').innerHTML = '';
                                    }
                                    else {
                                        if (this.parentElement.childElementCount > ultimate_form_object[key]['count']) {
                                            this.parentElement.removeChild(this.parentElement.lastElementChild);
                                            this.parentElement.removeChild(this.parentElement.lastElementChild);
                                        }
                                    }
                                }
                            }
                            else if (/(^reg_exp)/.test(rules[i])) {
                                var reg_exp_value = rules[i].split(':')[0];
                                var reg_exp_array = rules[i].split(':')[1].split('\\**\\');
                                ultimate_form_object[key]['valid'] = ultimate_form_object[key]['valid'] || class_object.validate_object_test[reg_exp_value](this.value, reg_exp_array);
                                if (!ultimate_form_object[key]['valid']) {
                                    if (class_object.return_event_type(tag) === 'change') {
                                        if (this.dataset.error) {
                                            document.querySelector('[name="' + this.dataset.error + '"]').innerHTML = obj_messages[rules[i]];
                                        }
                                        else {
                                            if (this.parentElement.childElementCount > ultimate_form_object[key]['count']) {
                                                this.parentElement.lastChild.innerHTML = obj_messages[rules[i]];
                                            }
                                            else {
                                                var span = document.createElement('p');
                                                var br = document.createElement('br');
                                                span.style.color = 'red';
                                                span.innerHTML = obj_messages[reg_exp_value];
                                                this.parentElement.appendChild(br);
                                                this.parentElement.appendChild(span);
                                            }
                                        }
                                    }
                                    break;
                                }
                                else {
                                    if (this.dataset.error) {
                                        document.querySelector('[name="' + this.dataset.error + '"]').innerHTML = '';
                                    }
                                    else {
                                        if (this.parentElement.childElementCount > ultimate_form_object[key]['count']) {
                                            this.parentElement.removeChild(this.parentElement.lastElementChild);
                                            this.parentElement.removeChild(this.parentElement.lastElementChild);
                                        }
                                    }
                                }
                            }
                            else if (/(^equals)/.test(rules[i])) {
                                var min_length_valuee = rules[i].split(':')[0];
                                var min_length_countt = rules[i].split(':')[1];
                                ultimate_form_object[key]['valid'] = ultimate_form_object[key]['valid'] || class_object.validate_object_test[min_length_valuee](this.value, min_length_countt);
                                if (!ultimate_form_object[key]['valid']) {
                                    if (class_object.return_event_type(tag) === 'change' || class_object.return_event_type(tag) === 'keyup') {
                                        if (this.dataset.error) {
                                            document.querySelector('[name="' + this.dataset.error + '"]').innerHTML = obj_messages[min_length_valuee];
                                        }
                                        else {
                                            if (this.parentElement.childElementCount > ultimate_form_object[key]['count']) {
                                                this.parentElement.lastChild.innerHTML = obj_messages[min_length_valuee];
                                            }
                                            else {
                                                var span = document.createElement('p');
                                                var br = document.createElement('br');
                                                span.style.color = 'red';
                                                span.innerHTML = obj_messages[min_length_valuee];
                                                this.parentElement.appendChild(br);
                                                this.parentElement.appendChild(span);
                                            }
                                        }
                                    }
                                    break;
                                }
                                else {
                                    if (this.dataset.error) {
                                        document.querySelector('[name="' + this.dataset.error + '"]').innerHTML = '';
                                    }
                                    else {
                                        if (this.parentElement.childElementCount > ultimate_form_object[key]['count']) {
                                            this.parentElement.removeChild(this.parentElement.lastElementChild);
                                            this.parentElement.removeChild(this.parentElement.lastElementChild);
                                        }
                                    }
                                }
                            }
                            else if (/(^greater_than_or_equal)/.test(rules[i])) {
                                var rule = rules[i].split(':')[0];
                                var standard = rules[i].split(':')[1];
                                ultimate_form_object[key]['valid'] = ultimate_form_object[key]['valid'] || class_object.validate_object_test[rule](this.value, standard);
                                if (!ultimate_form_object[key]['valid']) {
                                    if (class_object.return_event_type(tag) === 'change' || class_object.return_event_type(tag) === 'keyup') {
                                        if (this.dataset.error) {
                                            document.querySelector('[name="' + this.dataset.error + '"]').innerHTML = obj_messages[rule];
                                        }
                                        else {
                                            if (this.parentElement.childElementCount > ultimate_form_object[key]['count']) {
                                                this.parentElement.lastChild.innerHTML = obj_messages[rule];
                                            }
                                            else {
                                                var span = document.createElement('p');
                                                var br = document.createElement('br');
                                                span.style.color = 'red';
                                                span.innerHTML = obj_messages[rule];
                                                this.parentElement.appendChild(br);
                                                this.parentElement.appendChild(span);
                                            }
                                        }
                                    }
                                    break;
                                }
                                else {
                                    if (this.dataset.error) {
                                        document.querySelector('[name="' + this.dataset.error + '"]').innerHTML = '';
                                    }
                                    else {
                                        if (this.parentElement.childElementCount > ultimate_form_object[key]['count']) {
                                            this.parentElement.removeChild(this.parentElement.lastElementChild);
                                            this.parentElement.removeChild(this.parentElement.lastElementChild);
                                        }
                                    }
                                }
                            }
                            else if (/(^greater_than)/.test(rules[i])) {
                                var rule = rules[i].split(':')[0];
                                var standard = rules[i].split(':')[1];
                                ultimate_form_object[key]['valid'] = ultimate_form_object[key]['valid'] || class_object.validate_object_test[rule](this.value, standard);
                                if (!ultimate_form_object[key]['valid']) {
                                    if (class_object.return_event_type(tag) === 'change' || class_object.return_event_type(tag) === 'keyup') {
                                        if (this.dataset.error) {
                                            document.querySelector('[name="' + this.dataset.error + '"]').innerHTML = obj_messages[rule];
                                        }
                                        else {
                                            if (this.parentElement.childElementCount > ultimate_form_object[key]['count']) {
                                                this.parentElement.lastChild.innerHTML = obj_messages[rule];
                                            }
                                            else {
                                                var span = document.createElement('p');
                                                var br = document.createElement('br');
                                                span.style.color = 'red';
                                                span.innerHTML = obj_messages[rule];
                                                this.parentElement.appendChild(br);
                                                this.parentElement.appendChild(span);
                                            }
                                        }
                                    }
                                    break;
                                }
                                else {
                                    if (this.dataset.error) {
                                        document.querySelector('[name="' + this.dataset.error + '"]').innerHTML = '';
                                    }
                                    else {
                                        if (this.parentElement.childElementCount > ultimate_form_object[key]['count']) {
                                            this.parentElement.removeChild(this.parentElement.lastElementChild);
                                            this.parentElement.removeChild(this.parentElement.lastElementChild);
                                        }
                                    }
                                }
                            }
                            else if (/(^less_than_or_equal)/.test(rules[i])) {
                                var rule = rules[i].split(':')[0];
                                var standard = rules[i].split(':')[1];
                                ultimate_form_object[key]['valid'] = ultimate_form_object[key]['valid'] || class_object.validate_object_test[rule](this.value, standard);
                                if (!ultimate_form_object[key]['valid']) {
                                    if (class_object.return_event_type(tag) === 'change' || class_object.return_event_type(tag) === 'keyup') {
                                        if (this.dataset.error) {
                                            document.querySelector('[name="' + this.dataset.error + '"]').innerHTML = obj_messages[rule];
                                        }
                                        else {
                                            if (this.parentElement.childElementCount > ultimate_form_object[key]['count']) {
                                                this.parentElement.lastChild.innerHTML = obj_messages[rule];
                                            }
                                            else {
                                                var span = document.createElement('p');
                                                var br = document.createElement('br');
                                                span.style.color = 'red';
                                                span.innerHTML = obj_messages[rule];
                                                this.parentElement.appendChild(br);
                                                this.parentElement.appendChild(span);
                                            }
                                        }
                                    }
                                    break;
                                }
                                else {
                                    if (this.dataset.error) {
                                        document.querySelector('[name="' + this.dataset.error + '"]').innerHTML = '';
                                    }
                                    else {
                                        if (this.parentElement.childElementCount > ultimate_form_object[key]['count']) {
                                            this.parentElement.removeChild(this.parentElement.lastElementChild);
                                            this.parentElement.removeChild(this.parentElement.lastElementChild);
                                        }
                                    }
                                }
                            }
                            else if (/(^less_than)/.test(rules[i])) {
                                var rule = rules[i].split(':')[0];
                                var standard = rules[i].split(':')[1];
                                ultimate_form_object[key]['valid'] = ultimate_form_object[key]['valid'] || class_object.validate_object_test[rule](this.value, standard);
                                if (!ultimate_form_object[key]['valid']) {
                                    if (class_object.return_event_type(tag) === 'change' || class_object.return_event_type(tag) === 'keyup') {
                                        if (this.dataset.error) {
                                            document.querySelector('[name="' + this.dataset.error + '"]').innerHTML = obj_messages[rule];
                                        }
                                        else {
                                            if (this.parentElement.childElementCount > ultimate_form_object[key]['count']) {
                                                this.parentElement.lastChild.innerHTML = obj_messages[rule];
                                            }
                                            else {
                                                var span = document.createElement('p');
                                                var br = document.createElement('br');
                                                span.style.color = 'red';
                                                span.innerHTML = obj_messages[rule];
                                                this.parentElement.appendChild(br);
                                                this.parentElement.appendChild(span);
                                            }
                                        }
                                    }
                                    break;
                                }
                                else {
                                    if (this.dataset.error) {
                                        document.querySelector('[name="' + this.dataset.error + '"]').innerHTML = '';
                                    }
                                    else {
                                        if (this.parentElement.childElementCount > ultimate_form_object[key]['count']) {
                                            this.parentElement.removeChild(this.parentElement.lastElementChild);
                                            this.parentElement.removeChild(this.parentElement.lastElementChild);
                                        }
                                    }
                                }
                            }
                            /**
                             else if(/(^greater_than_or_equal)/.test(rules[i])){
                            var rule = rules[i].split(':')[0];
                            var standard = rules[i].split(':')[1];

                            ultimate_form_object[key]['valid'] = ultimate_form_object[key]['valid'] || class_object.validate_object_test[rule](this.value,standard);

                            //ultimate_form_object[key]['valid'] = ultimate_form_object[key]['valid'] || class_object.validate_object_test[min_length_valuee](this.value,min_length_countt);
                            
                            if(!ultimate_form_object[key]['valid']){

                                if(this.dataset.error){
                            document.querySelector('[name="'+this.dataset.error+'"]').innerHTML=obj_messages[rule];
                                    }else{

                                        if(this.parentElement.childElementCount>ultimate_form_object[key]['count']){
                               
                                            this.parentElement.lastChild.innerHTML = obj_messages[rule];
                                         }else{
                                             var span = document.createElement('p');
                                             var br = document.createElement('br');
                                             span.style.color = 'red';
                                             span.innerHTML=obj_messages[rule];
                                             this.parentElement.appendChild(br);
                                             this.parentElement.appendChild(span);
                                         }

                                    }
                             
                             
                            break;
                            }else{

                                if(this.dataset.error){
                            document.querySelector('[name="'+this.dataset.error+'"]').innerHTML='';
                                }else{

                                    if(this.parentElement.childElementCount>ultimate_form_object[key]['count']){
                                        this.parentElement.removeChild(this.parentElement.lastElementChild);
                                        this.parentElement.removeChild(this.parentElement.lastElementChild);
                                      
                        
                                    }
                                }

                                        
                            }
                        }
                             */
                            else if (/(^notequals)/.test(rules[i])) {
                                var min_length_valuee = rules[i].split(':')[0];
                                var min_length_countt = rules[i].split(':')[1];
                                ultimate_form_object[key]['valid'] = ultimate_form_object[key]['valid'] || class_object.validate_object_test[min_length_valuee](this.value, min_length_countt);
                                if (!ultimate_form_object[key]['valid']) {
                                    if (class_object.return_event_type(tag) === 'change' || class_object.return_event_type(tag) === 'keyup') {
                                        if (this.dataset.error) {
                                            document.querySelector('[name="' + this.dataset.error + '"]').innerHTML = obj_messages[min_length_valuee];
                                        }
                                        else {
                                            if (this.parentElement.childElementCount > ultimate_form_object[key]['count']) {
                                                this.parentElement.lastChild.innerHTML = obj_messages[min_length_valuee];
                                            }
                                            else {
                                                var span = document.createElement('p');
                                                var br = document.createElement('br');
                                                span.style.color = 'red';
                                                span.innerHTML = obj_messages[min_length_valuee];
                                                this.parentElement.appendChild(br);
                                                this.parentElement.appendChild(span);
                                            }
                                        }
                                    }
                                    break;
                                }
                                else {
                                    if (this.dataset.error) {
                                        document.querySelector('[name="' + this.dataset.error + '"]').innerHTML = '';
                                    }
                                    else {
                                        if (this.parentElement.childElementCount > ultimate_form_object[key]['count']) {
                                            this.parentElement.removeChild(this.parentElement.lastElementChild);
                                            this.parentElement.removeChild(this.parentElement.lastElementChild);
                                        }
                                    }
                                }
                            }
                            else if (rules[i] == 'valid_number') {
                                ultimate_form_object[key]['valid'] = ultimate_form_object[key]['valid'] || class_object.validate_object_test[rules[i]](this.value);
                                if (!ultimate_form_object[key]['valid']) {
                                    if (this.return_event_type(tag) === 'change') {
                                        if (this.dataset.error) {
                                            document.querySelector('[name="' + this.dataset.error + '"]').innerHTML = obj_messages[rules[i]];
                                        }
                                        else {
                                            if (this.parentElement.childElementCount > ultimate_form_object[key]['count']) {
                                                this.parentElement.lastChild.innerHTML = obj_messages[rules[i]];
                                            }
                                            else {
                                                var span = document.createElement('p');
                                                var br = document.createElement('br');
                                                span.style.color = 'red';
                                                span.innerHTML = obj_messages[rules[i]];
                                                this.parentElement.appendChild(br);
                                                this.parentElement.appendChild(span);
                                            }
                                        }
                                    }
                                    break;
                                }
                                else {
                                    if (this.dataset.error) {
                                        document.querySelector('[name="' + this.dataset.error + '"]').innerHTML = '';
                                    }
                                    else {
                                        if (this.parentElement.childElementCount > ultimate_form_object[key]['count']) {
                                            this.parentElement.removeChild(this.parentElement.lastElementChild);
                                            this.parentElement.removeChild(this.parentElement.lastElementChild);
                                        }
                                    }
                                }
                            }
                        }
                    });
                    this.addEventListener('blur', function () {
                        var rules = ultimate_form_object[key]['rules'];
                        var obj_messages = class_object.create_error_messages(ultimate_form_object, key);
                        for (var i = 0; i < rules.length; i++) {
                            ultimate_form_object[key]['valid'] = false;
                            if (rules[i] == 'required') {
                                ultimate_form_object[key]['valid'] = ultimate_form_object[key]['valid'] || class_object.validate_object_test[rules[i]](this.value);
                                if (!ultimate_form_object[key]['valid']) {
                                    if (this.dataset.error) {
                                        document.querySelector('[name="' + this.dataset.error + '"]').innerHTML = obj_messages[rules[i]];
                                    }
                                    else {
                                        if (this.parentElement.childElementCount > ultimate_form_object[key]['count']) {
                                            this.parentElement.lastChild.innerHTML = obj_messages[rules[i]];
                                        }
                                        else {
                                            var span = document.createElement('p');
                                            var br = document.createElement('br');
                                            span.style.color = 'red';
                                            span.innerHTML = obj_messages[rules[i]];
                                            this.parentElement.appendChild(br);
                                            this.parentElement.appendChild(span);
                                        }
                                    }
                                    break;
                                }
                                else {
                                    if (this.dataset.error) {
                                        document.querySelector('[name="' + this.dataset.error + '"]').innerHTML = '';
                                    }
                                    else {
                                        if (this.parentElement.childElementCount > ultimate_form_object[key]['count']) {
                                            this.parentElement.removeChild(this.parentElement.lastElementChild);
                                            this.parentElement.removeChild(this.parentElement.lastElementChild);
                                        }
                                    }
                                }
                                continue;
                            }
                            else if (/(^min_length_check)/.test(rules[i])) {
                                var min_length_value = rules[i].split(':')[0];
                                var min_length_count = rules[i].split(':')[1];
                                ultimate_form_object[key]['valid'] = ultimate_form_object[key]['valid'] || class_object.validate_object_test[min_length_value](this.value, min_length_count);
                                if (!ultimate_form_object[key]['valid']) {
                                    if (this.dataset.error) {
                                        document.querySelector('[name="' + this.dataset.error + '"]').innerHTML = obj_messages[min_length_value];
                                        // console.log(obj_messages[min_length_value]);
                                        // console.log(obj_messages);
                                    }
                                    else {
                                        if (this.parentElement.childElementCount > ultimate_form_object[key]['count']) {
                                            this.parentElement.lastChild.innerHTML = obj_messages[min_length_value];
                                        }
                                        else {
                                            var span = document.createElement('p');
                                            var br = document.createElement('br');
                                            span.style.color = 'red';
                                            span.innerHTML = obj_messages[min_length_value];
                                            this.parentElement.appendChild(br);
                                            this.parentElement.appendChild(span);
                                        }
                                    }
                                    break;
                                }
                                else {
                                    if (this.dataset.error) {
                                        document.querySelector('[name="' + this.dataset.error + '"]').innerHTML = '';
                                    }
                                    else {
                                        if (this.parentElement.childElementCount > ultimate_form_object[key]['count']) {
                                            this.parentElement.removeChild(this.parentElement.lastElementChild);
                                            this.parentElement.removeChild(this.parentElement.lastElementChild);
                                        }
                                    }
                                }
                            }
                            else if (rules[i] == 'valid_email') {
                                ultimate_form_object[key]['valid'] = ultimate_form_object[key]['valid'] || class_object.validate_object_test[rules[i]](this.value);
                                if (!ultimate_form_object[key]['valid']) {
                                    if (this.dataset.error) {
                                        document.querySelector('[name="' + this.dataset.error + '"]').innerHTML = obj_messages[rules[i]];
                                    }
                                    else {
                                        if (this.parentElement.childElementCount > ultimate_form_object[key]['count']) {
                                            this.parentElement.lastChild.innerHTML = obj_messages[rules[i]];
                                        }
                                        else {
                                            var span = document.createElement('p');
                                            var br = document.createElement('br');
                                            span.style.color = 'red';
                                            span.innerHTML = obj_messages[rules[i]];
                                            this.parentElement.appendChild(br);
                                            this.parentElement.appendChild(span);
                                        }
                                    }
                                    break;
                                }
                                else {
                                    if (this.dataset.error) {
                                        document.querySelector('[name="' + this.dataset.error + '"]').innerHTML = '';
                                    }
                                    else {
                                        if (this.parentElement.childElementCount > ultimate_form_object[key]['count']) {
                                            this.parentElement.removeChild(this.parentElement.lastElementChild);
                                            this.parentElement.removeChild(this.parentElement.lastElementChild);
                                        }
                                    }
                                }
                            }
                            else if (/(^reg_exp)/.test(rules[i])) {
                                var reg_exp_value = rules[i].split(':')[0];
                                var reg_exp_array = rules[i].split(':')[1].split('\\**\\');
                                ultimate_form_object[key]['valid'] = ultimate_form_object[key]['valid'] || class_object.validate_object_test[reg_exp_value](this.value, reg_exp_array);
                                if (!ultimate_form_object[key]['valid']) {
                                    if (this.dataset.error) {
                                        document.querySelector('[name="' + this.dataset.error + '"]').innerHTML = obj_messages[reg_exp_value];
                                    }
                                    else {
                                        if (this.parentElement.childElementCount > ultimate_form_object[key]['count']) {
                                            this.parentElement.lastChild.innerHTML = obj_messages[rules[i]];
                                        }
                                        else {
                                            var span = document.createElement('p');
                                            var br = document.createElement('br');
                                            span.style.color = 'red';
                                            span.innerHTML = obj_messages[reg_exp_value];
                                            this.parentElement.appendChild(br);
                                            this.parentElement.appendChild(span);
                                        }
                                    }
                                    break;
                                }
                                else {
                                    if (this.dataset.error) {
                                        document.querySelector('[name="' + this.dataset.error + '"]').innerHTML = '';
                                    }
                                    else {
                                        if (this.parentElement.childElementCount > ultimate_form_object[key]['count']) {
                                            this.parentElement.removeChild(this.parentElement.lastElementChild);
                                            this.parentElement.removeChild(this.parentElement.lastElementChild);
                                        }
                                    }
                                }
                            }
                            else if (/(^equals)/.test(rules[i])) {
                                var min_length_valuee = rules[i].split(':')[0];
                                var min_length_countt = rules[i].split(':')[1];
                                ultimate_form_object[key]['valid'] = ultimate_form_object[key]['valid'] || class_object.validate_object_test[min_length_valuee](this.value, min_length_countt);
                                if (!ultimate_form_object[key]['valid']) {
                                    if (this.dataset.error) {
                                        document.querySelector('[name="' + this.dataset.error + '"]').innerHTML = obj_messages[min_length_valuee];
                                    }
                                    else {
                                        if (this.parentElement.childElementCount > ultimate_form_object[key]['count']) {
                                            this.parentElement.lastChild.innerHTML = obj_messages[min_length_valuee];
                                        }
                                        else {
                                            var span = document.createElement('p');
                                            var br = document.createElement('br');
                                            span.style.color = 'red';
                                            span.innerHTML = obj_messages[min_length_valuee];
                                            this.parentElement.appendChild(br);
                                            this.parentElement.appendChild(span);
                                        }
                                    }
                                    break;
                                }
                                else {
                                    if (this.dataset.error) {
                                        document.querySelector('[name="' + this.dataset.error + '"]').innerHTML = '';
                                    }
                                    else {
                                        if (this.parentElement.childElementCount > ultimate_form_object[key]['count']) {
                                            this.parentElement.removeChild(this.parentElement.lastElementChild);
                                            this.parentElement.removeChild(this.parentElement.lastElementChild);
                                        }
                                    }
                                }
                            }
                            else if (/(^less_than_or_equal)/.test(rules[i])) {
                                var rule = rules[i].split(':')[0];
                                var standard = rules[i].split(':')[1];
                                ultimate_form_object[key]['valid'] = ultimate_form_object[key]['valid'] || class_object.validate_object_test[rule](this.value, standard);
                                if (!ultimate_form_object[key]['valid']) {
                                    if (this.dataset.error) {
                                        document.querySelector('[name="' + this.dataset.error + '"]').innerHTML = obj_messages[rule];
                                    }
                                    else {
                                        if (this.parentElement.childElementCount > ultimate_form_object[key]['count']) {
                                            this.parentElement.lastChild.innerHTML = obj_messages[rule];
                                        }
                                        else {
                                            var span = document.createElement('p');
                                            var br = document.createElement('br');
                                            span.style.color = 'red';
                                            span.innerHTML = obj_messages[rule];
                                            this.parentElement.appendChild(br);
                                            this.parentElement.appendChild(span);
                                        }
                                    }
                                    break;
                                }
                                else {
                                    if (this.dataset.error) {
                                        document.querySelector('[name="' + this.dataset.error + '"]').innerHTML = '';
                                    }
                                    else {
                                        if (this.parentElement.childElementCount > ultimate_form_object[key]['count']) {
                                            this.parentElement.removeChild(this.parentElement.lastElementChild);
                                            this.parentElement.removeChild(this.parentElement.lastElementChild);
                                        }
                                    }
                                }
                            }
                            else if (/(^less_than)/.test(rules[i])) {
                                var rule = rules[i].split(':')[0];
                                var standard = rules[i].split(':')[1];
                                ultimate_form_object[key]['valid'] = ultimate_form_object[key]['valid'] || class_object.validate_object_test[rule](this.value, standard);
                                if (!ultimate_form_object[key]['valid']) {
                                    if (this.dataset.error) {
                                        document.querySelector('[name="' + this.dataset.error + '"]').innerHTML = obj_messages[rule];
                                    }
                                    else {
                                        if (this.parentElement.childElementCount > ultimate_form_object[key]['count']) {
                                            this.parentElement.lastChild.innerHTML = obj_messages[rule];
                                        }
                                        else {
                                            var span = document.createElement('p');
                                            var br = document.createElement('br');
                                            span.style.color = 'red';
                                            span.innerHTML = obj_messages[rule];
                                            this.parentElement.appendChild(br);
                                            this.parentElement.appendChild(span);
                                        }
                                    }
                                    break;
                                }
                                else {
                                    if (this.dataset.error) {
                                        document.querySelector('[name="' + this.dataset.error + '"]').innerHTML = '';
                                    }
                                    else {
                                        if (this.parentElement.childElementCount > ultimate_form_object[key]['count']) {
                                            this.parentElement.removeChild(this.parentElement.lastElementChild);
                                            this.parentElement.removeChild(this.parentElement.lastElementChild);
                                        }
                                    }
                                }
                            }
                            else if (/(^greater_than_or_equal)/.test(rules[i])) {
                                var rule = rules[i].split(':')[0];
                                var standard = rules[i].split(':')[1];
                                ultimate_form_object[key]['valid'] = ultimate_form_object[key]['valid'] || class_object.validate_object_test[rule](this.value, standard);
                                if (!ultimate_form_object[key]['valid']) {
                                    if (this.dataset.error) {
                                        document.querySelector('[name="' + this.dataset.error + '"]').innerHTML = obj_messages[rule];
                                    }
                                    else {
                                        if (this.parentElement.childElementCount > ultimate_form_object[key]['count']) {
                                            this.parentElement.lastChild.innerHTML = obj_messages[rule];
                                        }
                                        else {
                                            var span = document.createElement('p');
                                            var br = document.createElement('br');
                                            span.style.color = 'red';
                                            span.innerHTML = obj_messages[rule];
                                            this.parentElement.appendChild(br);
                                            this.parentElement.appendChild(span);
                                        }
                                    }
                                    break;
                                }
                                else {
                                    if (this.dataset.error) {
                                        document.querySelector('[name="' + this.dataset.error + '"]').innerHTML = '';
                                    }
                                    else {
                                        if (this.parentElement.childElementCount > ultimate_form_object[key]['count']) {
                                            this.parentElement.removeChild(this.parentElement.lastElementChild);
                                            this.parentElement.removeChild(this.parentElement.lastElementChild);
                                        }
                                    }
                                }
                            }
                            else if (/(^greater_than)/.test(rules[i])) {
                                var rule = rules[i].split(':')[0];
                                var standard = rules[i].split(':')[1];
                                ultimate_form_object[key]['valid'] = ultimate_form_object[key]['valid'] || class_object.validate_object_test[rule](this.value, standard);
                                if (!ultimate_form_object[key]['valid']) {
                                    if (this.dataset.error) {
                                        document.querySelector('[name="' + this.dataset.error + '"]').innerHTML = obj_messages[rule];
                                    }
                                    else {
                                        if (this.parentElement.childElementCount > ultimate_form_object[key]['count']) {
                                            this.parentElement.lastChild.innerHTML = obj_messages[rule];
                                        }
                                        else {
                                            var span = document.createElement('p');
                                            var br = document.createElement('br');
                                            span.style.color = 'red';
                                            span.innerHTML = obj_messages[rule];
                                            this.parentElement.appendChild(br);
                                            this.parentElement.appendChild(span);
                                        }
                                    }
                                    break;
                                }
                                else {
                                    if (this.dataset.error) {
                                        document.querySelector('[name="' + this.dataset.error + '"]').innerHTML = '';
                                    }
                                    else {
                                        if (this.parentElement.childElementCount > ultimate_form_object[key]['count']) {
                                            this.parentElement.removeChild(this.parentElement.lastElementChild);
                                            this.parentElement.removeChild(this.parentElement.lastElementChild);
                                        }
                                    }
                                }
                            }
                            else if (/(^notequals)/.test(rules[i])) {
                                var min_length_valuee = rules[i].split(':')[0];
                                var min_length_countt = rules[i].split(':')[1];
                                ultimate_form_object[key]['valid'] = ultimate_form_object[key]['valid'] || class_object.validate_object_test[min_length_valuee](this.value, min_length_countt);
                                if (!ultimate_form_object[key]['valid']) {
                                    if (this.dataset.error) {
                                        document.querySelector('[name="' + this.dataset.error + '"]').innerHTML = obj_messages[min_length_valuee];
                                    }
                                    else {
                                        if (this.parentElement.childElementCount > ultimate_form_object[key]['count']) {
                                            this.parentElement.lastChild.innerHTML = obj_messages[min_length_valuee];
                                        }
                                        else {
                                            var span = document.createElement('p');
                                            var br = document.createElement('br');
                                            span.style.color = 'red';
                                            span.innerHTML = obj_messages[min_length_valuee];
                                            this.parentElement.appendChild(br);
                                            this.parentElement.appendChild(span);
                                        }
                                    }
                                    break;
                                }
                                else {
                                    if (this.dataset.error) {
                                        document.querySelector('[name="' + this.dataset.error + '"]').innerHTML = '';
                                    }
                                    else {
                                        if (this.parentElement.childElementCount > ultimate_form_object[key]['count']) {
                                            this.parentElement.removeChild(this.parentElement.lastElementChild);
                                            this.parentElement.removeChild(this.parentElement.lastElementChild);
                                        }
                                    }
                                }
                            }
                            else if (rules[i] == 'valid_number') {
                                ultimate_form_object[key]['valid'] = ultimate_form_object[key]['valid'] || class_object.validate_object_test[rules[i]](this.value);
                                if (!ultimate_form_object[key]['valid']) {
                                    if (this.dataset.error) {
                                        document.querySelector('[name="' + this.dataset.error + '"]').innerHTML = obj_messages[rules[i]];
                                    }
                                    else {
                                        if (this.parentElement.childElementCount > ultimate_form_object[key]['count']) {
                                            this.parentElement.lastChild.innerHTML = obj_messages[rules[i]];
                                        }
                                        else {
                                            var span = document.createElement('p');
                                            var br = document.createElement('br');
                                            span.style.color = 'red';
                                            span.innerHTML = obj_messages[rules[i]];
                                            this.parentElement.appendChild(br);
                                            this.parentElement.appendChild(span);
                                        }
                                    }
                                    break;
                                }
                                else {
                                    if (this.dataset.error) {
                                        document.querySelector('[name="' + this.dataset.error + '"]').innerHTML = '';
                                    }
                                    else {
                                        if (this.parentElement.childElementCount > ultimate_form_object[key]['count']) {
                                            this.parentElement.removeChild(this.parentElement.lastElementChild);
                                            this.parentElement.removeChild(this.parentElement.lastElementChild);
                                        }
                                    }
                                }
                            }
                        }
                    });
                };
            }
        }
    };
    return Form_Validator;
}());
